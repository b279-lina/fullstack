import { Container } from "react-bootstrap"

export default function Banner({data}) {
    const {title, content} = data

    return (
        <Container className="text-center banner-background mt-3">
            <h1 className="pt-5 banner-h1">{title}</h1>
            <p className="banner-p pt-3">{content}</p>
        </Container>
    )
}