import { useContext, useEffect, useState } from 'react'
import UserContext from '../UserContext'
import { CDBSidebar, CDBSidebarContent, CDBSidebarHeader, CDBSidebarMenuItem, CDBSidebarMenu } from 'cdbreact'
import { NavLink } from 'react-router-dom'

export default function AppSideNav() {
    const {user} = useContext(UserContext)

    const [id, setId] = useState('')
    const [isAdmin, setIsAdmin] = useState('')
    const [email, setEmail] = useState('')

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {Authorization: `Bearer ${user.token}`}
        }).then(res => res.json()).then(data => {
            setId(data._id)
            setEmail(data.email)
            setIsAdmin(data.isAdmin)
        })
    },[id, isAdmin, email, user.token])

    return(
        (isAdmin) ?
        <div className='side-bar'>
            <CDBSidebar className='side-bar-background'>
                <CDBSidebarHeader prefix={<i className='fa fa-bars fa-large'></i>}>
                    {email}
                </CDBSidebarHeader>
                <CDBSidebarContent>
                <CDBSidebarMenu>
                    <NavLink to='/profile'>
                        <CDBSidebarMenuItem icon='user-circle'>Profile</CDBSidebarMenuItem>
                    </NavLink>
                    <NavLink to='/allorders'>
                        <CDBSidebarMenuItem icon='shopping-cart'>All Orders</CDBSidebarMenuItem>
                    </NavLink>
                    <NavLink to='/allproducts'>
                        <CDBSidebarMenuItem icon='shopping-bag'>All Products</CDBSidebarMenuItem>
                    </NavLink>
                    <NavLink to='/addproduct'>
                        <CDBSidebarMenuItem icon='cart-plus'>Add Products</CDBSidebarMenuItem>
                    </NavLink>
                    <NavLink to='/allusers'>
                        <CDBSidebarMenuItem icon='user-alt'>Users</CDBSidebarMenuItem>
                    </NavLink>
                </CDBSidebarMenu>
            </CDBSidebarContent>
            </CDBSidebar> 
        </div> :
        <div className='side-bar'>
        <CDBSidebar className='side-bar-background'>
            <CDBSidebarHeader prefix={<i className='fa fa-bars fa-large'></i>}>
                {email}
            </CDBSidebarHeader>
            <CDBSidebarContent>
            <CDBSidebarMenu>
                <NavLink to='/profile'>
                    <CDBSidebarMenuItem icon='th-large'>Profile</CDBSidebarMenuItem>
                </NavLink>
                <NavLink to='/orders'>
                    <CDBSidebarMenuItem icon='shopping-bag'>Orders</CDBSidebarMenuItem>
                </NavLink>
            </CDBSidebarMenu>
        </CDBSidebarContent>
        </CDBSidebar>
    </div>
    )
}