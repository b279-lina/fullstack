import { useContext } from "react"
import { Button, Card } from "react-bootstrap"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function UserCard({userProp}){
    const {user} = useContext(UserContext)
    const {_id, email, isAdmin} = userProp


    let t = ''

    if(isAdmin === true){
        t = 'True'
    } else {
        t = 'False'
    }

    function makeAdmin() {
        fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/makeAdmin`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            }
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: `${email} is now an admin`,
                showConfirmButton: 'false',
                timer: '1500'
            })
        })
    }
    
    function removeAdmin() {
        fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/removeAdmin`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            }
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: `${email} is now a normal user`,
                showConfirmButton: 'false',
                timer: '1500'
            })
        })
    }

    return(
        (t === 'True') ? 
        <Card className="my-3">
            <Card.Body>
                <Card.Subtitle>Email:</Card.Subtitle>
                <Card.Text>{email}</Card.Text>
                <Card.Subtitle>isAdmin:</Card.Subtitle>
                <Card.Text>{t}</Card.Text>
                <Card.Text><Button onClick={removeAdmin}>Remove Admin</Button></Card.Text>
            </Card.Body>
        </Card>:
        <Card className="my-3">
        <Card.Body>
            <Card.Subtitle>Email:</Card.Subtitle>
            <Card.Text>{email}</Card.Text>
            <Card.Subtitle>isAdmin:</Card.Subtitle>
            <Card.Text>{t}</Card.Text>
            <Card.Text><Button variant='danger' onClick={makeAdmin}>Make Admin</Button></Card.Text>
        </Card.Body>
    </Card>
    )
}