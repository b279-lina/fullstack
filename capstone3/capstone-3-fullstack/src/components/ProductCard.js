import { Card } from 'react-bootstrap' 
import { Link } from 'react-router-dom'

export default function ProductCard({productProp}) {
    const {_id, name, description, price, stock} = productProp

    return(
        <Card className='my-3'>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle className='mt-5'>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Stock:</Card.Subtitle>
                <Card.Text>{stock}</Card.Text>
                <Link className='btn btn-primary' to={`/products/${_id}`}>View</Link>
            </Card.Body>
        </Card>
    )
}