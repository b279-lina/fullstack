import { useContext } from "react"
import { Button, Card } from "react-bootstrap"
import UserContext from "../UserContext"
import { Link } from "react-router-dom"
import Swal from 'sweetalert2'


export default function AllProductCard({allProductProp}){
    const {user} = useContext(UserContext)
    const {_id, name, description, price, stock, isActive} = allProductProp
    let t = ''

    if(isActive === true){
        t ='true'
    } else {
        t='false'
    }

    function archiveProduct() {
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            }
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Product archived successfully',
                showConfirmButton: 'false',
                timer: '1500'
            })
        })
    }

    function unarchiveProduct() {
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/unArchive`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            }
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Product archived successfully',
                showConfirmButton: 'false',
                timer: '1500'
            })
        })
    }

    return(
        (isActive) ? 
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle className='mt-5'>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Stock:</Card.Subtitle>
                <Card.Text>{stock}</Card.Text>
                <Card.Subtitle>Avialable:</Card.Subtitle>
                <Card.Text>{t}</Card.Text>
                <Card.Text><Button onClick={archiveProduct} variant="danger">Archive</Button></Card.Text>
                <Link className='btn btn-primary' to={`/update/${_id}`}>Update</Link>
            </Card.Body>
        </Card> :
        <Card className="my-3">
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className='mt-5'>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Subtitle>Stock:</Card.Subtitle>
            <Card.Text>{stock}</Card.Text>
            <Card.Subtitle>Avialable:</Card.Subtitle>
            <Card.Text>{t}</Card.Text>
            <Card.Text><Button onClick={unarchiveProduct} >Unarchive</Button></Card.Text>
            <Link className='btn btn-primary' to={`/update/${_id}`}>Update</Link>
        </Card.Body>
    </Card>
        
    )
}