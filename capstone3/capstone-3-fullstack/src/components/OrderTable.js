import { useState, useEffect } from "react"
import { Card } from "react-bootstrap"


export default function OrderTable({orderProp}){
    const {userId, products, totalAmount, purchasedON} = orderProp
    const [name, setName] = useState('')
    const [userEmail, setUserEmail] = useState('')

    let productId = ''
    let quantity = 0

    products.map(allProducts => {
        productId = allProducts.productId
        quantity = allProducts.quantity
    })

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`).then(res => res.json()).then(data => {
            setName(data.name)
        })
    }, [name, productId])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            setUserEmail(data.email)
        })
    }, [userId])

    return(
        <>
        <Card className="my-3">
            <Card.Body>
                <Card.Subtitle>User Email</Card.Subtitle>
                <Card.Text>{userEmail}</Card.Text>
                <Card.Subtitle>Products</Card.Subtitle>
                <Card.Text>{name}</Card.Text>
                <Card.Subtitle>Quantity</Card.Subtitle>
                <Card.Text>{quantity}</Card.Text>
                <Card.Subtitle>Total Amount</Card.Subtitle>
                <Card.Text>{totalAmount}</Card.Text>
                <Card.Subtitle>Purchase date</Card.Subtitle>
                <Card.Text>{purchasedON}</Card.Text>
            </Card.Body>
        </Card>
        </>
    )
}