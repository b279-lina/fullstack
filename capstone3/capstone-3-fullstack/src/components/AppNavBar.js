import { useContext } from "react"
import { Container, Nav, Navbar } from "react-bootstrap"
import { Link, NavLink } from "react-router-dom"
import UserContext from "../UserContext"

export default function AppNavBar() {
    const {user} = useContext(UserContext)

    
    return (
        <Container fluid className="p-0 fixed-top">
            <Navbar bg='dark' expand='lg'>
                <Container>
                    <Navbar.Brand className="brand" as={Link} to={'/'}>AniFigu</Navbar.Brand>
                    <Navbar.Toggle aria-controls="nav-items" />
                    <Navbar.Collapse id='nav-items'>
                    <Nav className='ms-auto'>
                        <Nav.Link className='navlink' as={NavLink} to={'/products'}>Products</Nav.Link>
                        {(user.token !== null) ? 
                        <Nav.Link className='navlink' as={NavLink} to={'/logout'}>Logout</Nav.Link> :
                        <>
                        <Nav.Link className='navlink' as={NavLink} to={'/login'}>Login</Nav.Link>
                        <Nav.Link className='navlink' as={NavLink} to={'/register'}>Registers</Nav.Link>
                        </>}
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </Container>
        
    )
}