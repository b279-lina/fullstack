import { useContext, useEffect, useState } from 'react'
import UserContext from '../UserContext'
import { Card, Col, Container, Row, Button } from 'react-bootstrap'
import { useParams, Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import AppSideNav from '../components/AppSideNav'

export default function ProductView(){
    const {user} = useContext(UserContext)
    const {productId} = useParams()
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    const [quantity, setQuantity] = useState(1)
    const [add, setAdd] = useState(true)
    const [dif, setDif] = useState(false)
    const [isActive, setIsActive] = useState(false)
    const [isAdmin, setIsAdmin] = useState(false)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {Authorization: `Bearer ${user.token}`}
        }).then(res => res.json()).then(data => {
            setIsAdmin(data.isAdmin)
        })
    },[isAdmin, user.token])


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`).then(res => res.json()).then(data => {
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setStock(data.stock)
        })
    }, [productId, name, description, price, stock])

    useEffect(() => {
        if(user.token === null || isAdmin === true){
            setIsActive(false)
        } else if(stock > 0){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [isActive, stock, isAdmin, user.token])

    function increase(){
        if(stock > quantity){
            setQuantity(quantity + 1)
            setAdd(true)
            setDif(true)
        } else {
            setAdd(false)
        }
    }

    function decrease(){
        if(quantity > 1){
            setQuantity(quantity - 1)
            setAdd(true)
        } else {
            setDif(false)
        }
    }

    function checkOutConfirmation(){
        Swal.fire({
            title: 'Are you sure?',
            text: "You are about to check out an item",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Check Out'
          }).then((result) => {
            if (user.token !== null) {
                checkOut()
            } else {
                <Navigate to='/' />
            }
          })
    }

    function checkOut(){
        fetch(`${process.env.REACT_APP_API_URL}/users/${productId}/checkOut`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            },
            body: JSON.stringify({
                quantity: quantity
            })
        }).then(res => {
            updateProductStock()
        })
    }

    function updateProductStock(){
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/updateStock`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                stock: stock - quantity
            })
        }).then(res => {
            <Navigate to={'/products'} />
            Swal.fire(
                'Ordered!',
                'Order Successful',
                'success'
              )
        })
    }

    return(
        (user.token !== null) ?
        <Row>
            <Col sm={1} md={1} lg={1}>
                <AppSideNav />
            </Col>
            <Col sm={11} md={11} lg={11}>
                <Container className='mt-5'>
                    <Row>
                        <Col lg={{span: 6, offset: 3}}>
                            <Card>
                                <Card.Body className='text-center'>
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Subtitle>Description:</Card.Subtitle>
                                    <Card.Text>{description}</Card.Text>
                                    <Card.Subtitle>Price:</Card.Subtitle>
                                    <Card.Text>{price}</Card.Text>
                                    <Card.Subtitle>Stock:</Card.Subtitle>
                                    <Card.Text>{stock}</Card.Text>
                                    <Card.Subtitle>Quantity:</Card.Subtitle>
                                    <Card.Text>{quantity}</Card.Text>
                                    {(add) ? <Button onClick={increase}>+</Button> :
                                    <Button variant='danger' onClick={increase} disabled>+</Button>}
                                    {(dif) ? <Button className='ms-2' onClick={decrease} >-</Button> :
                                    <Button className='ms-2' variant='danger' onClick={decrease} disabled>-</Button>}
                                    {(isActive) ? <Card.Text><Button className='my-5' onClick={checkOutConfirmation}>Check Out</Button></Card.Text> :
                                    <Card.Text><Button className='my-5' onClick={checkOutConfirmation} disabled>Check Out</Button></Card.Text>}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </Col>
        </Row> :
        <Container className='mt-5'>
        <Row>
            <Col lg={{span: 6, offset: 3}}>
                <Card>
                    <Card.Body className='text-center'>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Stock:</Card.Subtitle>
                        <Card.Text>{stock}</Card.Text>
                        <Card.Subtitle>Quantity:</Card.Subtitle>
                        <Card.Text>{quantity}</Card.Text>
                        {(add) ? <Button onClick={increase}>+</Button> :
                        <Button variant='danger' onClick={increase} disabled>+</Button>}
                        {(dif) ? <Button className='ms-2' onClick={decrease} >-</Button> :
                        <Button className='ms-2' variant='danger' onClick={decrease} disabled>-</Button>}
                        <Card.Text><Button className='my-5' onClick={checkOutConfirmation}>Check Out</Button></Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    )
}