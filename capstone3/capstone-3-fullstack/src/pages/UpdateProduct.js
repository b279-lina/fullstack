import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import { Navigate, useParams } from "react-router-dom"
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import AppSideNav from "../components/AppSideNav"
import Swal from 'sweetalert2'


export default function UpdateProduct() {
    const {user} = useContext(UserContext)
    const [isAdmin, setIsAdmin] = useState(true)
    const {productId} = useParams()
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {Authorization: `Bearer ${user.token}`}
        }).then(res => res.json()).then(data => {
            setIsAdmin(data.isAdmin)
        })
    },[isAdmin, user.token])

    useEffect(() => {
        if(name !== '' && description !== '' && price !== 0 && stock !== 0){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [isActive, name, description, price, stock])

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`).then(res => res.json()).then(data => {
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setStock(data.stock)
            })
        }
    }, [productId])

    function ProductUpdate() {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`,{
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Product updated successfully',
                showConfirmButton: 'false',
                timer: '1500'
        })
        })
    }


    return(
        (isAdmin === false || user.token === null) ?
        <Navigate to={'/'} /> :
        <>
        <Row>
            <Col sm={1} md={1} lg={1}>
                <AppSideNav />
            </Col>
            <Col className="text-center" sm={11} md={11} lg={11}>
                <Container>
                    <div className="container-box">
                        <Form onSubmit={e => ProductUpdate(e)}>
                            <Form.Group className='mb-3' controlId='name'>
                                <Form.Label className='login-label-font'>Update {name}</Form.Label>
                                <Form.Control type='text' placeholder='Product Name' onChange={e => setName(e.target.value)} value={name} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='description'>
                                <Form.Label className='login-label-font'>Description</Form.Label>
                                <Form.Control as='textarea' cols={30} row={10} placeholder='Description' onChange={e => setDescription(e.target.value)} value={description} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='price'>
                                <Form.Label className='login-label-font'>Price</Form.Label>
                                <Form.Control type='number' placeholder='Price' onChange={e => setPrice(e.target.value)} value={price} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='stock'>
                                <Form.Label className='login-label-font'>Stock</Form.Label>
                                <Form.Control type='number' placeholder='Stock' onChange={e => setStock(e.target.value)} value={stock} required />
                            </Form.Group>
                            { isActive ?
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='primary' type='submit' id='submitBtn'>Update</Button>
                                </Form.Group> :
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='danger' type='submit' id='submitBtn' disabled>Update</Button>
                                </Form.Group>
                            }
                        </Form>
                    </div>
                </Container>
            </Col>
        </Row>
        </>
    )
}