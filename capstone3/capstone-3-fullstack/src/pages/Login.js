import { useContext, useEffect, useState } from 'react'
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Login() {

    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password, isActive])

    const UserLogin = e => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/login`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => res.json()).then(data => {
            if(data.token !== undefined){
                setUser('token', localStorage.setItem('token', data.token)) 
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'You are now logged in',
                    showConfirmButton: 'false',
                    timer: '1500'
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Log in failed',
                    showConfirmButton: 'false',
                    timer: '1500'
                })
            }
        })
    }
    return(
        (user.token !== null) ? <Navigate to='/profile' />:
        <Container>
            <Row className='justify-content-md-center'>
                <Col className='text-center' sm={12} md={6}>
                    <div className='container-box'>
                    <Form onSubmit={e => UserLogin(e)}>
                        <Form.Group className='mb-3' controlId='email'>
                            <Form.Label className='login-label-font'>Email Addess:</Form.Label>
                            <Form.Control type='email' placeholder='Enter Email Address' onChange={e => setEmail(e.target.value)} value={email} required />
                        </Form.Group>

                        <Form.Group className='mb-3' controlId='password'>
                            <Form.Label className='login-label-font'>Password:</Form.Label>
                            <Form.Control type='password' placeholder='Password' onChange={e => setPassword(e.target.value)} value={password} required />
                        </Form.Group>

                        { isActive ?
                            <Form.Group className='justify-content-center-center'> 
                                <Button className='login-button' variant='primary' type='submit' id='submitBtn'>Login</Button>
                            </Form.Group> :
                            <Form.Group className='justify-content-center-center'> 
                                <Button className='login-button' variant='danger' type='submit' id='submitBtn' disabled>Login</Button>
                            </Form.Group>
                        }
                    </Form>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}