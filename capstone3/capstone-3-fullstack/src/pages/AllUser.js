import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import { Col, Row } from "react-bootstrap"
import UserCard from "../components/UserCard"
import { Navigate } from "react-router-dom"
import AppSideNav from "../components/AppSideNav"

export default function GetAllUser(){
    const {user} = useContext(UserContext)
    const [allUser, setAllUser] = useState('')
    const [isAdmin, setIsAdmin] = useState(true)

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/allUser`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                setAllUser(data.map(allUser => {
                    return(
                    <Col key={allUser._id} sm={11} md={11} lg={11}>
                        <UserCard userProp={allUser}/>
                    </Col>
                    )
                }))
            })
        }
    }, [allUser, user.token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {Authorization: `Bearer ${user.token}`}
        }).then(res => res.json()).then(data => {
            setIsAdmin(data.isAdmin)
        })
    },[isAdmin, user.token])

    return(
        (user.token === null || isAdmin === false) ?
        <Navigate to={'/'} /> :
        <Row>
            <Col sm={2} md={2} lg={2}>
                <AppSideNav />
            </Col>
            <Col sm={10} md={10} lg={10}>
                <h1 className="text-center my-5">All Users</h1>
            {[allUser]}
            </Col>
        </Row>
    )
}