import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom"
import OrderTable from '../components/OrderTable'
import { Col, Row } from "react-bootstrap"
import AppSideBar from '../components/AppSideNav'

export default function AllOrders() {
    const {user} = useContext(UserContext)
    const [isAdmin, setIsAdmin] = useState(true)
    const [allOrders, setAllOrders] = useState([])

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/user`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                    setIsAdmin(data.isAdmin)
                })
        }
    }, [isAdmin, user.token])

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                setAllOrders(data.map(allOrders => {
                    return(
                        <Col key={allOrders._id} sm={12} md={12} lg={12}>
                                <OrderTable orderProp={allOrders} />
                        </Col>
                    )
                }))
            })
        }
    }, [allOrders, user.token])

    return(
        (user.token === null || isAdmin === false) ?
        <Navigate to='/' /> :
        <Row>
            <Col md={2} sm={2} lg={2}>
            <AppSideBar />
            </Col>
            <Col sm={9} md={9} lg={9}>
                <h1 className="text-center my-5">All Orders</h1>
            {allOrders}
            </Col>
        </Row>
    )
}