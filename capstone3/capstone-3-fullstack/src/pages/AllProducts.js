import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import AllProductCard from "../components/AllProductCard"
import { Col, Container, Row } from 'react-bootstrap'
import AppSideNav from "../components/AppSideNav"
import { Navigate } from "react-router-dom"

export default function AllProducts() {
    const {user} = useContext(UserContext)
    const [allProducts, setAllProducts] = useState([])
    const [isAdmin, setIsAdmin] = useState(true)

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/user`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                setIsAdmin(data.isAdmin)
            })
        }
    }, [isAdmin, user.token])

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/allProducts`, {
                method: 'POST',
                headers: {
                    'Content-type': 'applications/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                setAllProducts(data.map(allProducts => {
                    return(
                        <Col key={allProducts._id} sm={12} md={6} lg={4}>
                            <AllProductCard allProductProp={allProducts} />
                        </Col>
                    )
                }))
            })
        }
    }, [allProducts, user.token])

    return(
        (user.token === null || isAdmin === false) ? 
        <Navigate to={'/'} />:
        <>
        <Row>
            <Col sm={1} md={1} lg={1}>
                <AppSideNav />
            </Col>
            <Col sm={11} md={11} lg={11}>
                <Container>
                    <h1 className="mt-3 text-center">All Products</h1>
                    <Row>{allProducts}</Row>
                </Container>
            </Col>
        </Row>
        </>
    )
}