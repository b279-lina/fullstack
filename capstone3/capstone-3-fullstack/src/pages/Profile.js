import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import AppSideNav from "../components/AppSideNav"
import { Navigate } from "react-router-dom"
import { Button, Col, Row, Table } from "react-bootstrap"

export default function Profile(){
    const {user} = useContext(UserContext)
    const [email, setEmail] = useState('')

    useEffect(() => {
        if(user.token !== null){
            fetch(`${process.env.REACT_APP_API_URL}/user`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${user.token}`
                }
            }).then(res => res.json()).then(data => {
                setEmail(data.email)
            })
        }
    }, [email, user.token])

    return (
        (user.token !== null) ?
        <>
            <Row>
                <Col sm={2} md={2} lg={2}>
                    <AppSideNav />
                </Col>
                <Col className="my-5" sm={9} md ={9} lg={9}>
                    <Table striped bordered hover size='sm'>
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{email}</td>
                            </tr>
                            <tr>
                                <td>Change Password</td>
                                <td><Button>test</Button></td>
                            </tr>
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </> :
        <Navigate to='/login' />
    )
}