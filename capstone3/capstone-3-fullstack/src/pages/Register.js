import { useContext, useEffect, useState } from 'react'
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register() {
    const {user} = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [isRegistered, setIsRegistered] = useState(false)

    useEffect(() =>{
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true)
        }
    }, [email, password1, password2, isActive])

    function userRegister(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/register`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            body:JSON.stringify({
                email: email,
                password: password1
            })
        }).then(res => res.json()).then(data => {
            if(data.email === false){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: `${email} is already registered`,
                    showConfirmButton: 'false',
                    timer: '1500'
                })
            } else {
                setIsRegistered(true)
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: `${email} registered successfully`,
                    showConfirmButton: 'false',
                    timer: '1500'
                })
            }
        })

        setEmail('')
        setPassword1('')
        setPassword2('')
    }

    return(
        (isRegistered) ? <Navigate to='/login' /> :
        (user.token !== null) ? <Navigate to='/' />:
            <Container>
                <Row className='justify-content-md-center'>
                    <Col className='text-center' sm={12} md={6}>
                        <div className='container-box'>
                        <Form onSubmit={e => userRegister(e)}>
                            <Form.Group className='mb-3' controlId='email'>
                                <Form.Label className='login-label-font'>Email Addess:</Form.Label>
                                <Form.Control type='email' placeholder='Enter Email Address' onChange={e => setEmail(e.target.value)} value={email} required />
                            </Form.Group>

                            <Form.Group className='mb-3' controlId='password1'>
                                <Form.Label className='login-label-font'>Password:</Form.Label>
                                <Form.Control type='password' placeholder='Password' onChange={e => setPassword1(e.target.value)} value={password1} required />
                            </Form.Group>

                            <Form.Group className='mb-3' controlId='password2'>
                                <Form.Label className='login-label-font'>Confirm password:</Form.Label>
                                <Form.Control type='password' placeholder='Confirm Password' onChange={e => setPassword2(e.target.value)} value={password2} required />
                            </Form.Group>

                            { isActive ?
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='primary' type='submit' id='submitBtn'>Register</Button>
                                </Form.Group> :
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='danger' type='submit' id='submitBtn' disabled>Register</Button>
                                </Form.Group>
                            }
                        </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
    )
}