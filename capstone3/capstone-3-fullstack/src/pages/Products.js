import { useContext, useEffect, useState } from "react"
import ProductCard from '../components/ProductCard'
import { Col, Container, Row } from "react-bootstrap"
import AppSideNav from '../components/AppSideNav'
import UserContext from "../UserContext"


export default function Products() {
    const{user} = useContext(UserContext)
    const [activeProduct, setProduct] = useState([])
    
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`).then(res => res.json()).then(data => {
            setProduct(data.map(activeProduct => {
                return(
                <Col key={activeProduct._id} sm={12} md={6} lg={4}>
                    <ProductCard productProp={activeProduct}/>
                </Col>)
            }))
        })
    },[activeProduct])

    return(
        (user.token !== null) ?
        <>
        <Row>
            <Col sm={1} md={1} lg={1}>
                <AppSideNav />
            </Col>
            <Col sm={11} md={11} lg={11}>
                <Container>
                    <h1 className="mt-3 text-center">Products</h1>
                    <Row>{activeProduct}</Row> 
                </Container>
            </Col>
        </Row>
        </>:
        <>
        <Container>
            <h1 className="mt-3 text-center">Products</h1>
            <Row>
                {activeProduct}
            </Row>
            </Container>
        </>
    )
}