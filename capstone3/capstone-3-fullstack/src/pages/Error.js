import Banner from "../components/Banner";

export default function Error(){
    const data = {
        title: '404 - NOT FOUND!',
        content: `Can't find ${window.location.href}`
    }
    return(
        <Banner data={data} />
    )
}