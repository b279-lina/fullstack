import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom"
import AppSideNav from "../components/AppSideNav"
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function AddProduct(){
    const {user} = useContext(UserContext)
    const [isAdmin, setIsAdmin] = useState(true)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)
    const [stock, setStock] = useState(0)
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            }
        }).then(res => res.json()).then(data => {
            setIsAdmin(data.isAdmin)
        })
    }, [isAdmin, user.token])

    function NewProduct(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/create`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        }).then(res => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Product added successfully',
                showConfirmButton: 'false',
                timer: '1500'
        })
        })
    }

    useEffect(() => {
        if(name !== '' && description !== '' && price !== 0 && stock !== 0){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [isActive, name, description, price, stock])

    return(
        (user.token === null || isAdmin === false) ?
        <Navigate to={'/'} /> :
        <>
        <Row>
            <Col sm={6} md={4} lg={2}>
                <AppSideNav />
            </Col>
            <Col className="text-center" sm={6} md={8} lg={10}>
                <Container>
                    <h1 className="mt-5">Add products</h1>
                    <div className="container-box">
                        <Form onSubmit={e => NewProduct(e)}>
                            <Form.Group className='mb-3' controlId='name'>
                                <Form.Label className='login-label-font'>Product Name</Form.Label>
                                <Form.Control type='text' placeholder='Product Name' onChange={e => setName(e.target.value)} value={name} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='description'>
                                <Form.Label className='login-label-font'>Description</Form.Label>
                                <Form.Control as='textarea' cols={30} row={10} placeholder='Description' onChange={e => setDescription(e.target.value)} value={description} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='price'>
                                <Form.Label className='login-label-font'>Price</Form.Label>
                                <Form.Control type='number' placeholder='Price' onChange={e => setPrice(e.target.value)} value={price} required />
                            </Form.Group>
                            <Form.Group className='mb-3' controlId='stock'>
                                <Form.Label className='login-label-font'>Stock</Form.Label>
                                <Form.Control type='number' placeholder='Stock' onChange={e => setStock(e.target.value)} value={stock} required />
                            </Form.Group>
                            { isActive ?
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='primary' type='submit' id='submitBtn'>Add Product</Button>
                                </Form.Group> :
                                <Form.Group className='justify-content-center-center'> 
                                    <Button className='login-button' variant='danger' type='submit' id='submitBtn' disabled>Add Product</Button>
                                </Form.Group>
                            }
                        </Form>
                    </div>
                </Container>
            </Col>
        </Row>
        </>
    )
}