import Banner from "../components/Banner"
import { useState, useEffect, useContext } from "react"
import { Col, Container, Row } from "react-bootstrap"
import Highlight from "../components/Highlight"
import AppSideNav from "../components/AppSideNav"
import UserContext from "../UserContext"

export default function Home() {
    const {user} = useContext(UserContext)

    const data = {
        title: 'AniFigu',
        content: 'Buy the latest and legit anime merchandise here!'
    }

    const [activeProduct, setProduct] = useState([])
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`).then(res => res.json()).then(data => {
            setProduct(data.slice(0, 3).map(activeProduct => {
                return(
                <Col key={activeProduct._id} sm={12} md={6} lg={4}>
                    <Highlight productProp={activeProduct}/>
                </Col>)
            }))
        })
    },[activeProduct])

    return(
        (user.token !== null) ?
        <>
            <Row>
                <Col sm={1} md={1} lg={1}>
                    <AppSideNav />
                </Col>
                <Col sm={11} md={11} lg={11}>
                        <Banner data={data} />
                        <Container>
                        <Row className="mt-5">{activeProduct}</Row>
                        </Container>
                </Col>
            </Row>
        </> :
        <>
        <Container>
        <Banner data={data} />
            <Row>
                {activeProduct}
            </Row>
        </Container>
        </>
    )
}