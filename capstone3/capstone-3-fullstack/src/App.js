import AppNavBar from './components/AppNavBar'
import { useState } from 'react'
import './App.css'
import {Route, Routes} from 'react-router-dom'
import { BrowserRouter as Router} from 'react-router-dom'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Products from './pages/Products'
import { UserProvider } from './UserContext'
import Logout from './pages/Logout'
import Error from './pages/Error'
import ProductView from './pages/ProductView'
import Profile from './pages/Profile'
import AllOrders from './pages/AllOrders'
import UserOrder from './pages/UserOrder'
import AllProducts from './pages/AllProducts'
import AddProduct from './pages/AddProduct'
import UpdateProduct from './pages/UpdateProduct'
import GetAllUser from './pages/AllUser'

function App() {
  const [user, setUser] = useState({
    token: localStorage.getItem('token')
  })

  const unsetUser = () => {
    localStorage.clear()
  }
  
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/products' element={<Products />} />
            <Route path='/products/:productId' element={<ProductView />} />
            <Route path='/profile' element={<Profile />} />
            <Route path='/allorders' element={<AllOrders />} />
            <Route path='/orders' element={<UserOrder />} />
            <Route path='/allproducts' element={<AllProducts />} />
            <Route path='/addproduct' element={<AddProduct />} /> 
            <Route path='/update/:productId' element={<UpdateProduct />} />
            <Route path='/allusers' element={<GetAllUser />} />
            <Route path='*' element={<Error />} />
          </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
