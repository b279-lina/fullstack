const txtFirstName = document.getElementById('txt-first-name')
const spanFullName = document.getElementById('span-full-name')
const txtLastName = document.getElementById('txt-last-name')

const updateFullName = () =>{
    let firstName = txtFirstName.value
    let lastName = txtLastName.value

    spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtLastName.addEventListener('keyup', updateFullName)
txtLastName.addEventListener('keyup', updateFullName)