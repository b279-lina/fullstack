import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Login(){
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        // Validation to enable register button
        if(email !== "" && password !== ""){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    },[email, password])

    const loginUser = e => {
        e.preventDefault()

        // Process a fetch request to the corresponding API
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        }) .then(res => res.json()).then(data => {
            console.log(data)

            // If no user info found, the "access" property will not be available
            if(typeof data.access !== undefined){
                localStorage.setItem('token', data.access)
                retrieveUserDeatails(data.access)

                Swal.fire({
                    icon: 'success',
                    title: 'Login Successful!',
                    text: 'Welcome to Zuitt!',
                  })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Authentication Failed!',
                    text: 'Please try again!',
                  })
            }
        })

        // setUser({
        //     // Store the email in the localStorage
        //     email: localStorage.setItem('email', email)
        // })
        
    
        // Clear input fields
        setEmail('')
        setPassword('')
    }

    // Retrieve user details using its token
    const retrieveUserDeatails = token => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(res => res.json()).then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                email: data.email
            })
        })
    }
    
    return(
        (user.token !== null) ? 
           <Navigate to='/courses' /> :
        <>
            <h1>Login</h1>
            <Form className='my-5' onSubmit={e => loginUser(e)}>
            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" onChange={e => setEmail(e.target.value)} value={email} required />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} value={password} required />
            </Form.Group>

            {/* Conditionally render submit button based on isActive state */}
            { isActive ?
            <Button variant="success" type="submit" id="submitBtn">
                Login
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn" disabled>
                Login
            </Button>
            }
            
        </Form>
        </>
    )
}