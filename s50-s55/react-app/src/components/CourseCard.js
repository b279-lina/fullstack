import {Card, Button} from 'react-bootstrap' 
import { Link } from 'react-router-dom'
// In React.js we have 3 hooks 
/* 
1. useState
2. useEffect
3. useContext
*/ 
import { useState } from 'react'

export default function CourseCard({courseProp}) {
    // Checks if props was succesfully passed
    console.log(courseProp)
    // Checks the type of the passed data
    console.log(typeof courseProp)

    // Destructuring the course Prop into their own variable
    const {_id, name, description, price} = courseProp

    // useState
    // Used for storing different states
    // used to keep track of information to individual components

    // Syntax -> const [getter, setter] = useState(initialGetterValue)

    // const[count, setCount] = useState(0)
    // const[total, difCount] = useState(30)
    // console.log(useState(0))

    // // Function that keeps track of the enrollees for a course

    // function enroll(){
        
    //     if(count >= 30 && total === 0){
    //         alert('No more seats')
    //     } else {
    //         setCount(count + 1)
    //         difCount(total - 1)
    //     }

    //     console.log('Enrollees: ' + count)
    // }

    return(
        <Card className='my-3'>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className='btn btn-primary' to={`/courseView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}